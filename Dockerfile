FROM docker:19.03 as static-docker-source

FROM python:3-alpine

# Note: Latest version of kubectl may be found at: https://github.com/kubernetes/kubernetes/releases
ENV KUBERNETES_VERSION="v1.18.2"
# Note: Latest version of helm may be found at: https://github.com/kubernetes/helm/releases
ENV HELM_VERSION="v3.2.1"

# Note: Latest version of terraform may be found at: https://releases.hashicorp.com/terraform/
ENV TERRAFORM_VERSION="0.12.25"
# Note: Latest version of tflint may be found at: https://github.com/terraform-linters/tflint/releases
ENV TFLINT_VERSION="0.16.0"
# Note: Latest version of terraform-docs may be found at: https://github.com/segmentio/terraform-docs/releases
ENV TERRAFORM_DOCS_VERSION=v0.9.1

# Note: Latest version of docker-credential-ecr-login may be found at: https://github.com/awslabs/amazon-ecr-credential-helper/releases
ENV DOCKER_CREDENTIAL_ECR_LOGIN_VERSION="0.3.1"

# Add binaries to PATH
ENV PATH $PATH:/root/google-cloud-sdk/bin

RUN apk update && \
  apk upgrade && \
  apk add --no-cache \
    bash \
    ca-certificates \
    git \
    curl \
    jq \
    openssl \
    openssh-client \
    unzip \
    rsync \
    tar \
    wget \
    zip

# gcloud SDK
RUN curl -sSL https://sdk.cloud.google.com | bash
RUN gcloud components install kubectl core gsutil 

# skopeo
RUN apk add --no-cache skopeo

# docker
COPY --from=static-docker-source /usr/local/bin/docker /usr/local/bin/docker

ADD https://storage.googleapis.com/container-diff/latest/container-diff-linux-amd64 /usr/local/bin/container-diff
RUN chmod 0755 /usr/local/bin/container-diff
ADD https://amazon-ecr-credential-helper-releases.s3.us-east-2.amazonaws.com/${DOCKER_CREDENTIAL_ECR_LOGIN_VERSION}/linux-amd64/docker-credential-ecr-login /usr/local/bin/docker-credential-ecr-login
RUN chmod 0755 /usr/local/bin/docker-credential-ecr-login

# helm
RUN wget -q https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm
RUN chmod 0755 /usr/local/bin/helm

# terraform
RUN cd /tmp && \
    wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/local/bin && \
    rm -rf /tmp/*
RUN chmod 0755 /usr/local/bin/terraform

# terraform lint
RUN cd /tmp && \
    wget https://github.com/terraform-linters/tflint/releases/download/v${TFLINT_VERSION}/tflint_linux_amd64.zip && \
    unzip tflint_linux_amd64.zip -d /usr/local/bin && \
    rm -rf /tmp/*
RUN chmod 0755 /usr/local/bin/tflint

# terraform-docs
RUN wget https://github.com/segmentio/terraform-docs/releases/download/${TERRAFORM_DOCS_VERSION}/terraform-docs-${TERRAFORM_DOCS_VERSION}-linux-amd64 -O /usr/local/bin/terraform-docs
RUN chmod 0755 /usr/local/bin/terraform-docs

# aws cli
RUN pip3 install awscli

# bumpversion
RUN pip3 install bumpversion

# ansible
RUN apk add --no-cache --virtual build-dependencies \
    python3-dev libffi-dev openssl-dev build-base \
    && pip3 install --upgrade pip \
    && pip3 install ansible ansible-lint \
    && apk del build-dependencies \
    && rm -rf /var/cache/apk/*
RUN mkdir -p /etc/ansible \
    && echo "[local]\nlocalhost ansible_connection=local" > /etc/ansible/hosts


