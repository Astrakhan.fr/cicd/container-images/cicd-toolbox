# cicd-toolbox

## Purpose

Provides a **all-in-one container image** with all the CLI or binaries you need when implementing your CI/CD pipelines.

## Usage

```yaml
your-job:
  image: registry.gitlab.com/astrakhan.fr/cicd/container-images/cicd-toolbox
  script:
    # YOUR CI CD COMMAND LINES
    - aws sts get-caller-identity
    - terraform validate
    - docker build .
    - ...
```

## Why a all-in-one container image ?

When you implement GitLab CI pipelines, sometime you want to use CLI from different tools or binaries. For instance to build and push a container image to AWS ECR, you need both `docker` and `AWS CLI`. Sometime packages like `curl` or `jq` are missing while you use a `terraform` or `ansible` container image. In this case, you may decide to:

- use the `docker` or `terraform` or `ansible` container image and install the missing packages or binaries like `AWS CLI` at the beginning of the job
- split your job into two seperate jobs where each job use its own container image

Downloading and installing the packages or binaries everytime a job runs is a waste of time. It could even be a waste of ressources when you have to install a python library or a npm module. That needs extra memory, while you just want to execute a simple command line using the `AWS CLI`. 

Splitting a job in two jobs takes a bit more time to be scheduled and run. But above all, you have to use the Artifacts or the GitLab Registry to share results of the first job with the second one, which once again is a waste of time. Morever the two container images used by the two jobs may not share the same layers. As a result, the GitLab Runners have to download two full images instead of one (that means more network traffic and disk space usage on the runner's nodes). 

Of course you can build a new container image everytime you want to use tow different tools, but it seems to be more convenient to **have only one container image, that brings all what you need to implement CI/CD in a single container image**. 


